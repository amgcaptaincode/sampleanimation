package com.upax.sampleanimation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnReact;
    private ReactionView reactionView;
    private ReactionPopup reactionPopup;
    private ReactionButton reactionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnReact = findViewById(R.id.btnReact);
        reactionView = findViewById(R.id.reactionView);
        //reactionView = new ReactionView(getApplicationContext());
        //reactionButton = (ReactionButton) findViewById(R.id.reactionButton);

        reactionPopup = new ReactionPopup(this, getReactions());

        reactionPopup.addReactionSelectedListener(new ReactionSelectedListener() {
            @Override
            public void onReactionSelected(ReactionModel reactionSelected) {
                showToast(reactionSelected.getText());
            }

            @Override
            public void onCancel() {
                showToast("onCancel");
            }
        });

        btnReact.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                //reactionView.show(motionEvent);
                //reactionPopup.show(view, motionEvent);
                reactionPopup.onTouch(view, motionEvent);
                return false;
            }
        });


    }

    private ArrayList<ReactionModel> getReactions() {
        ArrayList<ReactionModel> reactions = new ArrayList<>();
        reactions.add(new ReactionModel(
                1, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/Megusta.json", "", "Me gusta", "color"));
        reactions.add(new ReactionModel(
                2, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/Meencanta.json", "", "Me encanta", "color"));
        reactions.add(new ReactionModel(
                3, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/Medivierte.json", "", "Me divierte", "color"));
        reactions.add(new ReactionModel(
                4, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/asombra.json", "", "Me asombra", "color"));
        reactions.add(new ReactionModel(
                5, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/entristece.json", "", "Me entristece", "color"));
        reactions.add(new ReactionModel(
                6, "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/enojo.json", "", "Me enoja", "color"));
        /*reactions.add(new ReactionModel(
                4, "asombra.json", "", "Me asombra", "color"));
        reactions.add(new ReactionModel(
                5, "entristece.json", "", "Me entristece", "color"));
        reactions.add(new ReactionModel(
                6, "enojo.json", "", "Me enoja", "color"));*/
        return reactions;
    }

    private void showToast(String option) {
        Toast.makeText(getApplicationContext(), option, Toast.LENGTH_SHORT).show();
    }
}