package com.upax.sampleanimation;

import android.content.Context;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.PopupWindow;

import java.util.ArrayList;

public class ReactionPopup extends PopupWindow implements View.OnTouchListener {

    private ReactionModel reaction;
    private Context context;

    private ReactionView view;
    private FrameLayout frameLayout;

    public ReactionPopup(Context context, ArrayList<ReactionModel> reactions) {
        super(context);
        if (reactions == null || reactions.size() == 0) {
            throw new NullPointerException("ReactionModel cannot be null or size equal to zero");
        }
        init(context, reactions);
        this.context = context;
    }

    private void init(Context context, ArrayList<ReactionModel> reactions) {
        reaction = null;
        setElevation(12);
        view = new ReactionView(context, reactions);

        frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER));
        view.addReactionSelectedListener(new ReactionSelectedListener() {
            @Override
            public void onReactionSelected(ReactionModel reactionSelected) {
                reaction = reactionSelected;
                dismiss();
            }

            @Override
            public void onCancel() {
                dismiss();
            }
        });

        frameLayout.addView(view);

        setContentView(frameLayout);
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        //setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    /*public void show(View v, MotionEvent event) {
        if (!isShowing()) {
            showAtLocation(v, Gravity.NO_GRAVITY, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        view.show(event);
    }*/

    @Override
    public void dismiss() {
        view.dismiss();
        if (isShowing()) {
            super.dismiss();
        }
    }

    public void selectReaction(ReactionModel reaction) {
        view.selectReaction(reaction);
    }

    /*public Reaction getSelectedReaction() {
        return reaction;
    }*/

    public ReactionModel getSelectedReaction() {
        return reaction;
    }

    public Context getContext() {
        return context;
    }

    public ReactionView getReactionView() {
        return view;
    }

    public void addReactionSelectedListener(ReactionSelectedListener l) {
        view.addReactionSelectedListener(l);
    }

    public boolean removeReactionSelectedListener(ReactionSelectedListener l) {
        return view.removeReactionSelectedListener(l);
    }

    public void addVisibilityChangedListener(VisibilityChangedListener l) {
        view.addVisibilityChangedListener(l);
    }

    public boolean removeVisibilityChangedListener(VisibilityChangedListener l) {
        return view.removeVisibilityChangedListener(l);
    }

    @Override
    public boolean onTouch(View parent, MotionEvent event) {
        if (!isShowing()) {
            showAtLocation(parent, Gravity.NO_GRAVITY,
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            view.show(parent, event);
        }
        return view.onTouchEvent(event);
    }
}
