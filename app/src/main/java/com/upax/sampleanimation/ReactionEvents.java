package com.upax.sampleanimation;

public interface ReactionEvents {
    void onCancel();

    void onReactionSelected(ReactionModel reactionSelected);

}
