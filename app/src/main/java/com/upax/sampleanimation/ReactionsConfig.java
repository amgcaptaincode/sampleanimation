package com.upax.sampleanimation;

import android.content.Context;
import android.graphics.Color;

import java.util.ArrayList;

public class ReactionsConfig {

    private ArrayList<ReactionModel> reactions;
    private int reactionSize;
    private int horizontalMargin;
    private int verticalMargin;

    /** Horizontal gravity compare to parent view or screen */
    private PopupGravity popupGravity;

    /** Margin between dialog and screen border used by [PopupGravity] screen related values. */
    private int popupMargin;
    private int popupColor;

    /*// val reactionTextProvider: ReactionTextProvider,
    private Drawable textBackground;
    private int textColor;
    private int textHorizontalPadding;
    private int textVerticalPadding;
    private float textSize;*/

    private ReactionsConfig(ReactionsConfigBuilder builder) {
        reactions = builder.reactions;
        reactionSize = builder.reactionSize;
        horizontalMargin = builder.horizontalMargin;
        verticalMargin = builder.verticalMargin;
        popupGravity = builder.popupGravity;
        popupMargin = builder.popupMargin;
        popupColor = builder.popupColor;
    }

    public enum PopupGravity {
        /** Default position, similar to Facebook app. */
        DEFAULT,
        /** Align dialog left side with left side of the parent view. */
        PARENT_LEFT,
        /** Align dialog right side with right side of the parent view. */
        PARENT_RIGHT,
        /** Position dialog on left side of the screen. */
        SCREEN_LEFT,
        /** Position dialog on right side of the screen. */
        SCREEN_RIGHT,
        /** Position dialog on center of the screen. */
        CENTER
    }

    static class ReactionsConfigBuilder {

        private Context context;

        private ArrayList<ReactionModel> reactions;
        private int reactionSize;
        private int horizontalMargin;
        private int verticalMargin;
        private PopupGravity popupGravity;
        private int popupMargin;
        private int popupColor;
        /*private Drawable textBackground;
        private int textColor;
        private int textHorizontalPadding;
        private int textVerticalPadding;
        private float textSize;*/

        public ReactionsConfigBuilder(Context context) {
            this.context = context;
            reactions = new ArrayList<>();
            reactionSize = context.getResources().getDimensionPixelSize(R.dimen.reactions_item_size);
            horizontalMargin = context.getResources().getDimensionPixelSize(R.dimen.reactions_item_margin);
            verticalMargin = horizontalMargin;
            popupGravity = PopupGravity.DEFAULT;
            popupMargin = horizontalMargin;
            popupColor = Color.WHITE;
            /*textBackground = null;
            textColor = Color.WHITE;
            textHorizontalPadding = 0;
            textVerticalPadding = 0;
            textSize = 0f;*/

        }

        // Builder pattern
        public ReactionsConfigBuilder withReactions(ArrayList<ReactionModel> reactions) {
            this.reactions = reactions;
            return this;
        }

        public ReactionsConfigBuilder withReactionSize(int reactionSize) {
            this.reactionSize = reactionSize;
            return this;
        }

        public ReactionsConfigBuilder withHorizontalMargin(int horizontalMargin) {
            this.horizontalMargin = horizontalMargin;
            return this;
        }

        public ReactionsConfigBuilder withVerticalMargin(int verticalMargin) {
            this.verticalMargin = verticalMargin;
            return this;
        }

        public ReactionsConfigBuilder withPopupGravity(PopupGravity popupGravity) {
            this.popupGravity = popupGravity;
            return this;
        }

        public ReactionsConfigBuilder withPopupMargin(int popupMargin) {
            this.popupMargin = popupMargin;
            return this;
        }

        public ReactionsConfigBuilder withPopupColor( int popupColor) {
            this.popupColor = popupColor;
            return this;
        }

        ReactionsConfig build() {
            if (reactions == null) {
                throw new IllegalArgumentException("Empty reactions");
            }

            return new ReactionsConfig(this);
        }
    }

}
