package com.upax.sampleanimation;

public interface ReactionSelectedListener {

    void onReactionSelected(ReactionModel reactionSelected);

    void onCancel();
}
