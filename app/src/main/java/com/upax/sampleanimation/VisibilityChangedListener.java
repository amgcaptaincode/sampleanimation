package com.upax.sampleanimation;

public interface VisibilityChangedListener {
    void onShow();

    void onHide();
}
