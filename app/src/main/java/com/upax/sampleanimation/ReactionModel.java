package com.upax.sampleanimation;

public class ReactionModel {

    /*
    {
        "id": 99,
        "json": "https://redsocial-assets.s3.amazonaws.com/Reactions/looties/Megusta.json",
        "img": "https://redsocial-assets.s3.amazonaws.com/Reactions/like.png",
        "color": "#4D83FC",
        "type": "Me gusta"
    }
     */

    private int idReaction;
    private String fileName;
    private String image;
    private String text;
    private String color;

    public ReactionModel() {
    }

    public ReactionModel(int idReaction, String fileName, String image, String text, String color) {
        this.idReaction = idReaction;
        this.fileName = fileName;
        this.image = image;
        this.text = text;
        this.color = color;
    }

    public int getIdReaction() {
        return idReaction;
    }

    public void setIdReaction(int idReaction) {
        this.idReaction = idReaction;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
